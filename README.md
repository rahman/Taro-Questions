#  **基于流行前段跨端框架Taro的问答社区练习** 
## 目前只完成了前段
1. 简单的提问表单
2. 提问内容列表渲染
3. 提问点赞
## 运行方法 
git clone 拉下项目
进到根目录执行 npm run dev:weapp
使用微信开发工具打开即可看到效果
欢迎小伙伴们提出建议，拉取修改改
## 演示图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0614/142850_abe6c23d_1062657.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0614/142909_4d9362bf_1062657.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0614/142920_9a261cc6_1062657.png "屏幕截图.png")
