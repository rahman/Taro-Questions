import React, { Component } from 'react'
import Taro from '@tarojs/taro';
import { View,Text,Input,Teaxarea,Button,Image } from '@tarojs/components'
import './index.less'
import AddQuestion from './addquestion'
import Yes from '../../static/icon/zan.png'
import No from '../../static/icon/nozan.png'

//获取缓存
function getStore(key){
  let str = Taro.getStorageSync(key);
  if(!str){
    return [];
  }else
     return JSON.parse(str);
}

//设置缓存
function setStore(key,obj){
  let str = obj;
  if(typeof obj == 'object'){
    str = JSON.stringify(obj);
    try {
      Taro.setStorageSync(key,str)
    } 
    catch (e) { 
      console.log("catch:"+e)
    }
  }else{
    console.log("缓存失败")
    // Taro.setStorageSync(key,str);
  }
}

export default class Index extends Component {

  state = {
    showQuestionModal:false, //默认不显示QuestionModal
    questionlist:getStore('questions')
  }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  //弹出添加问题层
  addQuestion(){
    this.setState({showQuestionModal:true})
  }
  //取消添加问题层
  closeQuestion(){
    //在小程序中必须加on  成onCloseQuestion;H5中不需要
    this.setState({showQuestionModal:false})
  }
  receivesQusetions(options){
    let  {questionlist} = this.state;
    questionlist.push({id:parseInt(Math.random()*10000),...options});
    //异步毁掉的方式
    this.setState({questionlist:questionlist},()=>{
      console.log(this.state.questionlist);
      setStore('questions',this.state.questionlist)
    })
    this.closeQuestion();
  }
  //点在
  addVote(item){
    let {questionlist} = this.state;
    if(item){
      item.vote = item.vote?(item.vote+1):1;//处理点赞数
    }
    let newquestionlist =  questionlist.map(itemQuestion=>{
      if(itemQuestion.id == item.id){
        itemQuestion = {...item}
      }
      return itemQuestion;
    })
    this.setState({questionlist:newquestionlist},()=>{
      setStore('questions',this.state.questionlist)
    })
  }
  //取消点赞
  cutVote(item){
    let {questionlist} = this.state;
    if(item){
      item.vote = item.vote?((item.vote-1)>=0?(item.vote-1):0):0;//处理点赞数
    }
    let newquestionlist =  questionlist.map(itemQuestion=>{
      if(itemQuestion.id == item.id){
        itemQuestion = {...item}
      }
      return itemQuestion;
    })
    this.setState({questionlist:newquestionlist},()=>{
      setStore('questions',this.state.questionlist)
    })
  }
  render () {
    let {questionlist} = this.state;
    let mynewquestionlist = questionlist.sort((a,b)=>{
      return a.vote<b.vote ? 1 : -1;
    })
    return (
      <View className='index'>
        <View className="title">问答社区</View>
          <View className="question-list">
              {
                mynewquestionlist.map((item,index)=>{
                  return (<View key={item.id} className="quastions">
                            <View className="question-left">
                              <View className="question-title">{item.title}</View>
                              <View className="question-content">{item.content}</View>
                              <View className="question-questioner">提问人：{item.questioner}</View>
                            </View>
                            <View className="question-right">
                                  <Image onClick={this.addVote.bind(this,item)} className="icon yesicon" src={Yes} />
                                  <Text>{item.vote?item.vote:0}</Text>
                                  <Image onClick={this.cutVote.bind(this,item)} className="icon noicon" src={No} />
                            </View>
                        </View>)
                        
                })
              }
              
          </View>
        {this.state.showQuestionModal ? <AddQuestion onReceivesQusetions = {this.receivesQusetions.bind(this)} onCloseQuestion = { this.closeQuestion.bind(this) }/>:null}
        <Button className="question-btn" onClick={this.addQuestion.bind(this)}>提问</Button>
      </View>
    )
  }
}
