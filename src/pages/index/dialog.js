import { Component } from 'react'
import Taro,{components} from '@tarojs/taro';
import { View,Text,Input,Teaxarea,Button} from '@tarojs/components';
import './dialog.less'

export default class Dialog extends Component{
    render() {
        return(
            <View className="dialog">
                {this.props.children}
            </View>
        )   
    }
}