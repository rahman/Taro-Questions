import React, { Component } from 'react'
import Taro from '@tarojs/taro';
import { View,Text,Input,Textarea,Button} from '@tarojs/components';
import Dialog from './dialog';
import './addquestion.less'



export default class AddQuestion extends Component{

    confirmQuestion(){
        //点击确定是采集数据并且关闭弹出层
        // console.log("点击确定");
        // console.log(this.state.title)
        if(this.state.title&&this.state.content&&this.state.questioner){
            //调用父级组件传递数据
            this.props.onReceivesQusetions && this.props.onReceivesQusetions(this.state);
            // 提交表单
            Taro.showToast({
                title:"提交成功",
                icon:"none",
                duration:2000
            })
        }else{
            Taro.showToast({
                title:"必须字段不能为空",
                icon:"none",
                duration:2000
            })
        }

    }

    cancelQuestion(){
        //需要调用上层组件的方法
        this.props.onCloseQuestion&&this.props.onCloseQuestion();
        console.log("点击取消");
    }
    changeTitle(e){
        this.setState({title:e.target.value})
    }
    changeContent(e){
        this.setState({content:e.target.value})
    }
    changeQuestioner(e){
        this.setState({questioner:e.target.value})
    }
    render() {
        return(
            <Dialog>
                <View className="add-question">
                    <View className="question-body">
                        <Input focus className="question-title" onInput={this.changeTitle.bind(this)} id="title" name="title" placeholder="请输入问题标题" />
                        <Textarea focus className="question-content" onInput={this.changeContent.bind(this)} id="content" name="content" placeholder="请输入问题描述" />
                        <Input focus className="questioner" onInput={this.changeQuestioner.bind(this)} id="questioner" name="questioner" placeholder="请输入提问人昵称" />
                        <View className="btn-group">
                            <Button onClick={this.confirmQuestion.bind(this)} className="btn-question ok" >确定</Button>
                            <Button onClick={this.cancelQuestion.bind(this)} className="btn-question cancel">取消</Button>
                        </View>
                    </View>
                </View>
            </Dialog>
        )   
    }
}